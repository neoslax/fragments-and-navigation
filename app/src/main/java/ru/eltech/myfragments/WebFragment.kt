package ru.eltech.myfragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ru.eltech.myfragments.databinding.FragmentWebBinding

class WebFragment : Fragment(){

    private var _binding: FragmentWebBinding? = null
    private val binding: FragmentWebBinding
        get() = _binding ?: throw RuntimeException("FragmentWebBinding == null")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWebBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
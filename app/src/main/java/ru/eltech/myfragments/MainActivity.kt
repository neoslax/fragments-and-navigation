package ru.eltech.myfragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity(), LoginFragment.BottomBarEnabler {

    private lateinit var navBar: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navBar = findViewById<BottomNavigationView>(R.id.bottom_nav_bar)
        val navController = (supportFragmentManager.findFragmentById(R.id.main_container) as NavHostFragment).navController
        navBar.setupWithNavController(navController)
    }

    override fun setBottomBarVisible() {
        navBar.visibility = View.VISIBLE
    }
}